package com.zyplayer.doc.manage.service.manage.impl;

import com.zyplayer.doc.manage.repository.manage.entity.UserAuth;
import com.zyplayer.doc.manage.repository.manage.mapper.UserAuthMapper;
import com.zyplayer.doc.manage.service.manage.UserAuthService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 暮光：城中城
 * @since 2018-12-03
 */
@Service
public class UserAuthServiceImpl extends ServiceImpl<UserAuthMapper, UserAuth> implements UserAuthService {

}
