package com.zyplayer.doc.manage.service.manage.impl;

import com.zyplayer.doc.manage.repository.manage.entity.UserInfo;
import com.zyplayer.doc.manage.repository.manage.mapper.UserInfoMapper;
import com.zyplayer.doc.manage.service.manage.UserInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 暮光：城中城
 * @since 2018-12-03
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements UserInfoService {

}
