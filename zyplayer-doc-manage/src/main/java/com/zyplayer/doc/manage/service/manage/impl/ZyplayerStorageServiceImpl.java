package com.zyplayer.doc.manage.service.manage.impl;

import com.zyplayer.doc.manage.repository.manage.entity.ZyplayerStorage;
import com.zyplayer.doc.manage.repository.manage.mapper.ZyplayerStorageMapper;
import com.zyplayer.doc.manage.service.manage.ZyplayerStorageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 暮光：城中城
 * @since 2018-11-27
 */
@Service
public class ZyplayerStorageServiceImpl extends ServiceImpl<ZyplayerStorageMapper, ZyplayerStorage> implements ZyplayerStorageService {

}
