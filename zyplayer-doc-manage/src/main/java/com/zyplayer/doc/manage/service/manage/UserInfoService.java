package com.zyplayer.doc.manage.service.manage;

import com.zyplayer.doc.manage.repository.manage.entity.UserInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 暮光：城中城
 * @since 2018-12-03
 */
public interface UserInfoService extends IService<UserInfo> {

}
