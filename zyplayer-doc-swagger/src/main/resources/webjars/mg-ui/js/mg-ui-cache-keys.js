/**
 * 页面中所有使用到的缓存key必须定义在这里
 */
var cacheKeys = {
    userSettings: 'userSettings',
    swaggerResourcesList: 'swagger-resources-list',
    globalParamList: 'zyplayer-doc-global-param-list',
    pRequestObjStart: 'p-request-obj-',
    pSimulationResponse: 'p-simulation-response',
}